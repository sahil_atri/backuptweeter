/**
 * Created by sahil on 29/1/16.
 */

var statusCodes = require('../Config/constants').STATUS_CODES;


module.exports = function(code, message) {
    return {
        response: {
            message: message,
            data: null
        },
        statusCode: code
    };
}

module.exports.badRequest = function (message) {
    return {
        response: {
            message: message,
            data: null
        },
        statusCode: statusCodes.BAD_REQUEST
    };
}

module.exports.unauthorized = function(message) {
    return {
        response: {
            message: message,
            data: null
        },
        statusCode: statusCodes.UNAUTHORIZED
    };
}