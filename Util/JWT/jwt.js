/**
 * Created by sahil on 23/1/16.
 */


var Jwt = require('jsonwebtoken');
var boom = require('boom');

var secretKey = require('../../Config').constants.jwtPrivateKey;

/*both async functions, must use callback with these*/

var cipherToken = function (tokenData, callback) {
    /*arguments:- tokenData, key, options, callback*/
    Jwt.sign(tokenData, secretKey, {algorithm: 'HS256'}, function(token) {
        callback(token);
    });
};



var decipherToken = function(token, options, callback) {
    /*arguments :- token, key, options, callback, options ignored if not specified*/
    Jwt.verify(token, secretKey, options, function (err, decodedData) {
        if (err) {
            return callback(err);
        }
        callback(null, decodedData);
    })
}

/*both async*/
module.exports = {
    cipherToken: cipherToken,
    decipherToken: decipherToken
};
