/**
 * Created by sahil on 15/2/16.
 */

var bcrypt = require('bcryptjs');

module.exports.hash = function(target, callback) {//args to callback err, hash
    bcrypt.genSalt(10, function(err, salt) {
        if (err) {
            return callback(true);
        }
        bcrypt.hash(target, salt, callback);
    });
}


module.exports.compare = function(target, hash, callback) {//args to callback err, res(bool)
    bcrypt.compare(target, hash, callback);
}