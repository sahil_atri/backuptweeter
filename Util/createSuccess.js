/**
 * Created by sahil on 29/1/16.
 */



module.exports = function(code, message, result) {
    
    return {response: {message: message, data: result}, statusCode: code};
}