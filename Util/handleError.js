/**
 * Created by sahil on 4/2/16.
 */



var createError = require('./createError');
var errorMessages = require('../Config/responseMessages').errorMessages;
var statusCodes = require('../Config/constants').STATUS_CODES;
module.exports = function(err, callback) {
    if (err) {
        if (err.statusCode) {
            return callback(err);
        }
        if (err.code === 11000) {
            return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_ALREADY_EXISTS));
        }
        return callback(createError(statusCodes.BAD_REQUEST, errorMessages.BAD_REQUEST));
    }
}