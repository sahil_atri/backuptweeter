/**
 * Created by sahil on 29/2/16.
 */

var moment = require('moment');
module.exports = function(result, callback) {
    var last = [];
    var count = [0, 0, 0, 0];
    last[0] = moment().subtract(1, 'days')._d;
    last[1] = moment().subtract(1, 'weeks')._d;
    last[2] = moment().subtract(1, 'months')._d;
    var j = 0;
    for (var i = 0 ; i < result.length ; i ++) {
        if (result[i].date < last[j]) {
            j ++;
            count[j] = count[j - 1];
        }
        count[j] += result[i].count;
    }
    while (j < 3) {
        j ++;
        count[j] = count[j - 1];
    }
    var ans = {
        lastDay: count[0],
        lastWeek: count[1],
        lastMonth: count[2],
        lastYear: count[3]
    }
    callback(null, ans);
}