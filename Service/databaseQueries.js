/**
 * Created by sahil on 21/1/16.
 */

/*
---callback function format---

 function(err, document) {
    if (err) {
        return callback(err);
    }
    return callback(null, document);
 }

-------------------------------
 */
var find = function(collection, query, projection, callback) {
    collection.find(query, projection, {lean: true}, callback);
};

var findOne = function(collection, query, projection, callback) {
    collection.findOne(query, projection, {lean: true}, callback);
};

var updateData = function (collection, query, updateConditions, options, callback) {
    collection.update(query, updateConditions, options, function(err, result) {
        //console.log(err);
        
        callback(null, result);
    });
};

var addNewDoc = function(collection, document, callback) {
    var aDocument = new collection(document);
    aDocument.save(callback);
}


var deleteData = function (collection, query, callback) {
    collection.remove(query, callback);
};

var populateData = function(collection, query, projection, options, populateOptions, callback) {
    collection.find(query, projection, options).populate(populateOptions).exec(callback);
}

var aggregateData = function(collection, pipeline, callback) {
    collection.aggregate(pipeline, callback);
}

var findOneAndUpdateData = function(collection, query, updateConditions, options, callback) {
    collection.findOneAndUpdate(query, updateConditions, options, callback);
}
var findOneAndUpdateData = function(collection, query, updateConditions, options, callback) {
    collection.findOneAndUpdate(query, updateConditions, options, callback);
}

var aggregateAndPopulateData = function(collection, pipleline, populateOptions, callback) {
    collection.aggregate(pipleline).exec(function (err, result) {
        if (err) {
            return callback(err);
        }
        return collection.populate(result, populateOptions, callback);
    });
}

var countData = function(collection, query, callback) {
    collection.count(query, callback);
}

module.exports = {
    findData: find,
    findOneData: findOne,
    updateData: updateData,
    addData: addNewDoc,
    deleteData: deleteData,
    populateData: populateData,
    aggregateData: aggregateData,
    findOneAndUpdateData: findOneAndUpdateData,
    aggregateAndPopulateData: aggregateAndPopulateData,
    countData: countData
}