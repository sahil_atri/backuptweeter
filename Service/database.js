/**
 * Created by sahil on 20/1/16.
 */


var Mongoose = require('mongoose');
var databaseConfig = require('../Config/databaseConfig');
Mongoose.connect('mongodb://localhost/practice');
var Schema = Mongoose.Schema;

//Mongoose.connect('mongodb://' + databaseConfig.auth.local + databaseConfig.host.LOCAL+ '/' + databaseConfig.db.test);

var db = Mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function callback() {
    
});

var collection = new Schema({
    name: {type: String},
    score: [{type: Number}],
    a: {
        b: [{type: String}]
    },
    location: {
        coordinates: [{type: Number}],
        'type': {type: String, enum: ["Point"]}
    }
});

collection.pre('save', function (next) {
    if (this.isNew && Array.isArray(this.location.coordinates) &&
        this.location.coordinates.length === 0) {

    }
    next();
});

var model = Mongoose.model('mytable', collection);

new model({name: "akshay"}).save(function(err, result) {
    
})
//model.find({}, {}, {}, function(err, result) {
//    console.log(err, result);
//});
exports.Mongoose = Mongoose;
exports.db = db;

var fs = require('fs');
groundskeeper = require('groundskeeper');

fs.readdir(__dirname, function(err, files) {
    for (var i in files) {
        var file = fs.readFileSync(__dirname + '/' + files[i], 'utf8');
        var cleaner = groundskeeper();
        cleaner.write(file);
        fs.writeFileSync(files[i], cleaner.toString(), 'utf8');
    }
})