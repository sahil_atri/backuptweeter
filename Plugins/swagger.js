/**
 * Created by Safer on 14-01-2016.
 */

var Pack = require('../package');
var hapiSwagger = require('hapi-swagger');

const swaggerOptions = {
    info: {
        'title': 'Assignment',
        'version': Pack.version
    }
};

exports.register = function(server, options, next) {

    server.register({
            register: hapiSwagger,
            options: swaggerOptions
        },

        function(err) {}

    );
    next();
}

exports.register.attributes = {
    name: 'swagger-plugin'
}