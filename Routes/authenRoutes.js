/**
 * Created by sahil on 8/2/16.
 */


var Joi = require('joi');
var authenController = require('../Controller/AuthenController/authenController');
var PERSONNEL_TYPES = require('../Config/constants').PERSONNEL_TYPES;

var loginRoute = {
    method: 'POST',
    path: '/authen/login',

    config: {

        validate: {
            payload: {
                userid: Joi.string().required(),
                password: Joi.string().required(),
                type: Joi.string().valid(PERSONNEL_TYPES.ADMIN, PERSONNEL_TYPES.USER).required()
            }
        },

        handler: function(request, reply) {
            authenController.login(request.payload, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                var authToken = result.response.data.auth;
                result.response.data = null;
                return reply(result.response).code(result.statusCode).header('auth', authToken);
            });
        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags: ['api'],
        plugins: {
            'hapi-swagger': {
                payloadType: "form"
            }
        }
    }

}

var logoutRoute = {

    method: 'DELETE',
    path: '/authen/logout',

    config: {

        handler: function(request, reply) {
            authenController.logout(request.headers.auth, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        validate : {
            headers: Joi.object({
                auth: Joi.string()
            }).unknown()
        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags : ['api']

    }
}

module.exports = {
    loginRoute: loginRoute,
    logoutRoute: logoutRoute
}