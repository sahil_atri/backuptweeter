/**
 * Created by Safer on 11-01-2016.
 */

var authenRoutes = require('./authenRoutes');
var userRoutes = require('./userRoutes');
var adminRoutes = require('./adminRoutes');

module.exports = [];

for (var route in authenRoutes) {
    module.exports.push(authenRoutes[route]);
}

for (var route in userRoutes) {
    module.exports.push(userRoutes[route]);
}

for (var route in adminRoutes) {
    module.exports.push(adminRoutes[route]);
}
