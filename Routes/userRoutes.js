/**
 * Created by sahil on 3/2/16.
 */

var Joi = require('joi');
//Joi.objectId = require('joi-objectid')(Joi);
var userController = require('../Controller/UserController/userController');
var authenController = require('../Controller/AuthenController/authenController');
var RESPONSE_MESSAGES = require('../Config/responseMessages');
var FOLLOW_OPTIONS = require('../Config/constants').FOLLOW_OPTIONS;

/*
 ----------------------------------------
 FOLLOW-UNFOLLOW ROUTE
 ----------------------------------------
 */

var followUnfollowRoute = {
    method: 'PUT',
    path: '/user/followUnfollow',
    config: {

        handler: function(request, reply) {
            userController.followUnfollowUser(request.headers.auth, request.payload, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        validate : {

            headers: Joi.object({
                auth: Joi.string()
            }).unknown(),

            payload: {
                followee: Joi.string().required(),
                action: Joi.string().valid(FOLLOW_OPTIONS.FOLLOW, FOLLOW_OPTIONS.UNFOLLOW).required()
            }

        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags : ['api'],

        plugins: {
            'hapi-swagger': {
                payloadType: "form"//,
                //responses: RESPONSE_MESSAGES.DEFAULT_SWAGGER_MSGS
            }
        }

    }
};

/*
 ----------------------------------------
 TIMELINE ROUTE
 ----------------------------------------
 */

var timelineRoute = {
    method: 'GET',
    path: '/user/timeline',
    config: {

        handler: function(request, reply) {
            userController.getTimeline(request.headers.auth, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        validate : {
            headers: Joi.object({
                auth: Joi.string()
            }).unknown()
        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: Joi.array().items({
                    _id: Joi.object().keys({
                        _bsontype: Joi.string(),
                        id: Joi.string()
                    }),
                    createdBy: Joi.object().keys({
                        firstname: Joi.string()
                    }),
                    text: Joi.string(),
                    postedBy: Joi.object().keys({
                        firstname: Joi.string()
                    })
                })
            })
        },

        tags : ['api']

    }
};

/*
 ----------------------------------------
 POST TWEET ROUTE
 ----------------------------------------
 */

var postTweetRoute = {
    method: 'POST',
    path: '/user/postTweet',
    config: {

        handler: function(request, reply) {
            userController.postTweet(request.headers.auth, request.payload, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        validate : {

            headers: Joi.object({
                auth: Joi.string()
            }).unknown(),

            payload: {
                text: Joi.string().max(160).min(1)
            }

        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags : ['api']

    }
};

/*
 ----------------------------------------
 VIEW USER PROFILE ROUTE
 ----------------------------------------
 */

var userProfileRoute = {
    method: 'GET',
    path: '/user/profile/{userid}',
    config: {

        handler: function(request, reply) {
            userController.viewUserProfile(request.headers.auth, request.params.userid, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        validate : {

            headers: Joi.object({
                auth: Joi.string()
            }).unknown(),

            params: {
                userid: Joi.string().required()
            }

        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: Joi.array().items({
                    firstname: Joi.string(),
                    lastname: Joi.string(),
                    tweets: Joi.array().items(
                        Joi.object().keys({
                            text: Joi.string(),
                            timeCreated: Joi.date()
                        })
                    )
                })
            })
        },

        tags : ['api']

    }
};

/*
 ----------------------------------------
 EDIT INFO ROUTE
 ----------------------------------------
 */

editInfoRoute = {
    method: 'PUT',
    path: '/user/editinfo',
    config: {

        handler: function(request, reply) {
            userController.editUserInfo(request.headers.auth, request.payload, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        validate : {

            headers: Joi.object({
                auth: Joi.string()
            }).unknown(),

            payload: {
                firstname: Joi.string().max(20).min(1),
                lastname: Joi.string().max(20).min(1),
                userid: Joi.string(),
                password: Joi.string()
            }

        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags : ['api']

    }
};

/*
 ----------------------------------------
 COUNT TWEET ROUTE
 ----------------------------------------
 */

var countTweetsRoute = {
    method: 'GET',
    path: '/user/count',
    config: {

        handler: function(request, reply) {
            userController.countUserTweets(request.headers.auth, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        validate : {

            headers: Joi.object({
                auth: Joi.string()
            }).unknown()

        },

        tags : ['api']

    }
};

/*
 ----------------------------------------
 SIGNUP ROUTE
 ----------------------------------------
 */

var userSignupRoute = {
    method: 'POST',
    path: '/user/signup',
    config: {

        validate: {

            payload: {
                userid: Joi.string().required(),
                password: Joi.string().required(),
                firstname: Joi.string().required(),
                lastname: Joi.string().required()
            }

        },

        handler: function(request, reply) {
            authenController.signup(request.headers.auth, request.payload, 'user', function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags: ['api'],

        plugins: {
            'hapi-swagger': {
                payloadType: "form"
            }
        }

    }
};

/*
 ----------------------------------------
 RETWEET ROUTE
 ----------------------------------------
 */

var retweetRoute = {
    method: 'POST',
    path: '/user/retweet',
    config: {

        validate: {

            payload: {
                tweetId: Joi.string().required()
            },

            headers: Joi.object({
                auth: Joi.string()
            }).unknown()

        },

        handler: function(request, reply) {
            userController.retweet(request.headers.auth, request.payload, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags: ['api'],

        plugins: {
            'hapi-swagger': {
                payloadType: "form"
            }
        }

    }
};

/*
 ----------------------------------------
 LIKE UNLIKE ROUTE
 ----------------------------------------
 */

var likeUnlikeRoute = {
    method: 'POST',
    path: '/user/likeUnlikeTweet',
    config: {

        validate: {

            payload: {
                tweetId: Joi.string().required(),
                action: Joi.string().valid('like', 'unlike').required()
            },

            headers: Joi.object({
                auth: Joi.string()
            }).unknown()

        },

        handler: function(request, reply) {
            userController.likeUnlikeTweet(request.headers.auth, request.payload, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags: ['api'],

        plugins: {
            'hapi-swagger': {
                payloadType: "form"
            }
        }
    }
};

/*
 ----------------------------------------
 PIC UPLOAD ROUTE
 ----------------------------------------
 */

var uploadPicRoute = {
    method: 'POST',
    path: '/user/uploadPic',

    config: {

        plugins: {
            'hapi-swagger': {
                payloadType: 'form'
            }
        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags: ['api'],

        payload: {
            output: 'stream',
            parse: true,
            allow: 'multipart/form-data'
        },

        validate: {

            payload: {
                file: Joi.any()
                    .meta({ swaggerType: 'file' })
                    .description('json file').required()
            },

            headers: Joi.object({
                auth: Joi.string()
            }).unknown()

        },

        handler: function (request, reply) {
            userController.uploadDP(request.headers.auth, request.payload, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        }

    }
};

/*
 ----------------------------------------
 MY TWEETS ROUTE
 ----------------------------------------
 */

var myTweetsRoute = {
    method: 'GET',
    path: '/user/viewMyTweets',
    config: {

        validate: {

            query: {
                years: Joi.string().valid('0', '1', '2', '5', '10', '20', '50').required(),
                months: Joi.string().valid('0', '1', '2', '6', '9').required()
            },

            headers: Joi.object({
                auth: Joi.string()
            }).unknown()

        },

        handler: function(request, reply) {
            userController.myTweets(request.headers.auth, request.query, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: Joi.array().items({
                    text: Joi.string(),
                    createdBy: Joi.object().keys({
                        userid: Joi.string()
                    })
                })
            })
        },

        tags: ['api'],

        plugins: {
            'hapi-swagger': {
                payloadType: "form"
            }
        }
    }
};

/*
 ----------------------------------------
 MY LAST TWEETS ROUTE
 ----------------------------------------
 */

var myLastTweetsRoute = {
    method: 'GET',
    path: '/user/viewMyLastTweets',
    config: {

        validate: {

            headers: Joi.object({
                auth: Joi.string()
            }).unknown()

        },

        handler: function(request, reply) {
            userController.myLastTweets(request.headers.auth, request.query, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: Joi.object().keys({
                    lastDay: Joi.number(),
                    lastWeek: Joi.number(),
                    lastMonth: Joi.number(),
                    lastYear: Joi.number()
                })
            })
        },

        tags: ['api'],

        plugins: {
            'hapi-swagger': {
                payloadType: "form"
            }
        }
    }
};

/*
 ----------------------------------------
 PUSH NOTIFICATION ROUTE
 ----------------------------------------
 */

var pushNotificationRoute = {
    method: 'PUT',
    path: '/user/pushNotifications',
    config: {

        validate: {

            headers: Joi.object({
                auth: Joi.string()
            }).unknown(),

            payload: {
                message: Joi.string().required()
            }

        },

        handler: function(request, reply) {
            userController.pushNotification(request.headers.auth, request.payload, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags: ['api'],

        plugins: {
            'hapi-swagger': {
                payloadType: "form"
            }
        }
    }
};

var toCsv = {
    method: 'GET',
    path: 'getMeCSV/',
    config: {
        handler: function(request, reply){
            userController.getMeCSV(function(err, result) {
                if (err) {
                    reply()
                }

            });
        },
        tags: ['api']
    }
}

module.exports = {
    postTweetRoute: postTweetRoute,
    followUnfollowRoute: followUnfollowRoute,
    timelineRoute: timelineRoute,
    userProfileRoute: userProfileRoute,
    editInfoRoute: editInfoRoute,
    userSignupRoute: userSignupRoute,
    retweetRoute: retweetRoute,
    likeUnlikeRoute: likeUnlikeRoute,
    uploadPicRoute: uploadPicRoute,
    myTweetsRoute: myTweetsRoute,
    myLastTweetsRoute: myLastTweetsRoute,
    pushNotificationRoute: pushNotificationRoute
};