/**
 * Created by sahil on 3/2/16.
 */

var Joi = require('joi');
var authenController = require('../Controller/AuthenController/authenController');
var adminController = require('../Controller/AdminController/adminController');
var ADMIN_TYPES = require('../Config/constants').ADMIN_TYPES;
var FOLLOW_OPTIONS = require('../Config/constants').FOLLOW_OPTIONS;
/*
 ----------------------------------------
 ADMIN SIGNUP ROUTE
 ----------------------------------------
 */

var adminSignupRoute = {
    method: 'POST',
    path: '/admin/signup',
    config: {

        validate: {

            payload: {
                userid: Joi.string().required(),
                password: Joi.string().required(),
                firstname: Joi.string().required(),
                lastname: Joi.string().required(),
                adminType: Joi.string().valid(ADMIN_TYPES.ADMIN, ADMIN_TYPES.SUPER_ADMIN).required()
            }

        },

        handler: function(request, reply) {
            authenController.signup(request.headers.auth, request.payload, ADMIN_TYPES.ADMIN, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags: ['api'],

        plugins: {
            'hapi-swagger': {
                payloadType: "form"
            }
        }

    }
}

/*
 ----------------------------------------
 ADMIN VIEW USER
 ----------------------------------------
 */

var viewUserProfileRoute = {
    method: 'GET',
    path: '/admin/view_user_profile/{userid}',
    config: {

        handler: function(request, reply) {
            adminController.viewUser(request.headers.auth, request.params.userid, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        validate : {

            headers: Joi.object({
                auth: Joi.string()
            }).unknown(),

            params: {
                userid: Joi.string().required()
            }

        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: Joi.array().items({
                    firstname: Joi.string(),
                    lastname: Joi.string(),
                    tweets: Joi.array().items(
                        Joi.object().keys({
                            text: Joi.string(),
                            timeCreated: Joi.date()
                        })
                    )
                })
            })
        },

        tags : ['api']

    }
};

/*
 ----------------------------------------
 ADMIN EDIT USER
 ----------------------------------------
 */

var editUserProfileRoute = {
    method: 'PUT',
    path: '/admin/edituserprofile/{userid}',
    config: {

        handler: function(request, reply) {
            adminController.editUser(request.headers.auth, request.params.userid, request.payload, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        validate : {

            headers: Joi.object({
                auth: Joi.string()
            }).unknown(),

            params: {
                userid: Joi.string().required()
            },

            payload: {
                userid: Joi.string(),
                firstname: Joi.string(),
                lastname: Joi.string()
            }

        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags : ['api']

    }
};

/*
 ----------------------------------------
 ADMIN EDIT FOLLOW-UNFOLLOW
 ----------------------------------------
 */

var followUnfollowRoute = {
    method: 'PUT',
    path: '/admin/follow_unfollow/{userid}',
    config: {

        handler: function(request, reply) {
            adminController.followUnfollowUser(request.headers.auth, request.payload, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        validate : {

            headers: Joi.object({
                auth: Joi.string()
            }).unknown(),

            payload: {
                user: Joi.string().required(),
                followee: Joi.string().required(),
                action: Joi.string().valid(FOLLOW_OPTIONS.FOLLOW, FOLLOW_OPTIONS.UNFOLLOW).required()
            }

        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags : ['api'],

        plugins: {
            'hapi-swagger': {
                payloadType: "form"
            }
        }

    }
};

/*
 ----------------------------------------
 ADMIN DELETE USER
 ----------------------------------------
 */

var deleteUserRoute = {
    method: 'DELETE',
    path: '/admin/delete_user/{userid}',
    config: {

        handler: function(request, reply) {
            adminController.deleteUser(request.headers.auth, request.params.userid, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        validate : {

            headers: Joi.object({
                auth: Joi.string()
            }).unknown(),

            params: {
                userid: Joi.string().required()
            }

        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: null
            })
        },

        tags : ['api', 'admin']

    }
};

/*
 ----------------------------------------
 USER COUNT ROUTE
 ----------------------------------------
 */

var countUserRoute = {
    method: 'GET',
    path: '/admin/usercount',
    config: {

        handler: function(request, reply) {
            adminController.userCount(request.headers.auth, request.query, function(err, result) {
                if (err) {
                    return reply(err.response).code(err.statusCode);
                }
                return reply(result.response).code(result.statusCode);
            });
        },

        validate : {

            headers: Joi.object({
                auth: Joi.string()
            }).unknown(),

            query: {
                years: Joi.string().valid('0', '1', '2', '5', '10', '50', '100').required(),
                months: Joi.string().valid('0', '1', '2', '6', '9').required()
            }

        },

        response: {
            options: {
                allowUnknown: true
            },
            schema: Joi.object({
                message: Joi.string().required(),
                data: Joi.number().integer()
            })
        },

        tags : ['api', 'admin']

    }
};

module.exports = {
    adminSignupRoute: adminSignupRoute,
    viewUserProfileRoute: viewUserProfileRoute,
    editUserProfileRoute: editUserProfileRoute,
    userfollowUnfollowRoute: followUnfollowRoute,
    deleteUserRoute: deleteUserRoute,
    countUserRoute: countUserRoute
}