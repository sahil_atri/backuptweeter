/**
 * Created by sahil on 20/1/16.
 */


module.exports = {
    serverConfig: require('./serverConfig'),
    databaseConfig: require('./databaseConfig'),
    constants: require('./constants'),
    responseMessages: require('./responseMessages')
}