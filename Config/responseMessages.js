/**
 * Created by sahil on 1/2/16.
 */

module.exports.errorMessages = {
    BAD_REQUEST: 'bad request',
    UNKNOWN: 'unknown error',
    INVALID_CRED: 'username password mismatch',
    USER_NOT_FOUND: 'username not found',
    UNAUTHORIZED: 'unauthorized',
    USER_ALREADY_EXISTS: 'user already exists',
    USER_ALREADY_NOT_FOLLOWED: 'user is already not followed',
    USER_ALREADY_FOLLOWED: 'user is already followed',
    USER_ALREADY_DELETED: 'user is already deleted',
    TWEET_UNLIKED: 'tweet unliked successfully',
    TWEET_NOT_FOUND: 'tweet not found',
    TWEET_ALREADY_LIKED: 'tweet already liked',
    TWEET_ALREADY_RETWEETED: 'tweet already retweeted',
    TWEET_ALREADY_UNLIKED: 'tweet already unliked',
    BCRYPT_ERROR: 'bcrypt error',
    INVALID_FILE: 'file format not supported',
    GCM_FAILED: 'push notification error'
}


module.exports.successMessages = {
    LOGGED_IN: 'user logged in successfully',
    LOGGED_OUT: 'user logged out successfully',
    SIGNED_UP: 'user registered successfully',
    FOLLOWED: 'user followed successfully',
    TWEET_EXTRACTED: 'tweets extracted successfully',
    TWEET_POSTED: 'tweet posted successfully',
    COUNT: 'count calculated successfully',
    PROFILE_EXTRACTED: 'profile information and tweets returned',
    INFO_UPDATED: 'information updated successfully',
    UNFOLLOWED: 'user unfollowed successfully',
    SUCCESS: 'operation successful',
    USER_DELETED: 'user deleted successfully',
    TWEET_LIKED: 'tweet liked successfully',
    PIC_UPLOADED: 'profile pic uploaded successfully',
    NOTIF_PUSHED: 'notification pushed successfully'
}

module.exports.DEFAULT_SWAGGER_MSGS = {
    200: {description: 'OK'},
    201: {description: 'CREATED'},
    400: {description: 'BAD_REQUEST'},
    401: {description: 'UNAUTHORIZED'}
}