/**
 * Created by sahil on 1/2/16.
 */

module.exports.jwtPrivateKey = 'dontsharethiskey';

module.exports.STATUS_CODES = {
    OK: 200,
    CREATED: 201,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    ALREADY_EXISTS_CONFLICT: 409
};


module.exports.ERRORS = {
    JWTERROR: 'JsonWebTokenError',
    MONGOERROR: 'MongoError'
}

module.exports.FILE_PATHS = {
    PROFILE_PICS: '/home/sahil/WebstormProjects/untitled4/ProfilePics/'
}

module.exports.PIC_EXTS = {
    jpg: 'jpg',
    jpeg: 'jpeg',
    png: 'png'
}

module.exports.ADMIN_TYPES = {
    ADMIN: 'admin',
    SUPER_ADMIN: 'superadmin'
}

module.exports.FOLLOW_OPTIONS = {
    FOLLOW: 'follow',
    UNFOLLOW: 'unfollow'
}

module.exports.USER_MODEL_PROP = {
    userid: 'userid',
    password: 'password',
    firstname: 'firstname',
    lastname: 'lastname',
    followers: 'followers',
    following: 'following'
}

module.exports.LIKE_OPTIONS = {
    LIKE: 'like',
    UNLIKE: 'unlike'
}

module.exports.PERSONNEL_TYPES = {
    USER: 'user',
    ADMIN: 'admin'
}