/**
 * Created by sahil on 27/1/16.
 */


var Mongoose = require('mongoose')
var Schema = Mongoose.Schema;

var TweetSchema = new Schema({
    createdBy: {type: Schema.ObjectId},
    postedBy: [{type: Schema.ObjectId, ref: 'users', default: []}],
    likedBy: [{type: Schema.ObjectId, ref: 'users', default: []}],
    text: {type: String},
    timeCreated: {type: Date}
})


module.exports = Mongoose.model('tweets', TweetSchema);

