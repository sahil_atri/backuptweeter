/**
 * Created by sahil on 3/2/16.
 */

var Mongoose = require('mongoose')
var Schema = Mongoose.Schema;


var AdminSchema = new Schema({
    userid: {type: String, unique: true, index: true},   //unique username
    password: {type: String},
    firstname: {type: String},
    lastname: {type: String},
    token: [{type: String}],
    isDeleted: {type: Boolean, default: false},
    adminType: {type: String, enum: ["superadmin", "admin"]}
})

module.exports = Mongoose.model('admin', AdminSchema);
