/**
 * Created by sahil on 21/1/16.
 */

module.exports = {
    userModel: require('./userModel'),
    tweetModel: require('./tweetModel'),
    adminModel: require('./adminModel')
}
