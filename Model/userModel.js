/**
 * Created by sahil on 23/1/16.
 */


var Mongoose = require('mongoose')
var Schema = Mongoose.Schema;


var UserSchema = new Schema({
    userid: {type: String, unique: true, index: true},   //unique username
    password: {type: String},
    firstname: {type: String},
    lastname: {type: String},
    isDeleted: {type: Boolean, default: false},
    followers: [{type: Schema.ObjectId, ref: 'users'}],
    following: [{type: Schema.ObjectId, ref: 'users'}],
    tweets: [{type: Schema.ObjectId, ref: 'tweets'}],
    token: [{type: String}],
    profilePic: {type: String},
    timeCreated: {type: Date, default: new Date()},
    deviceToken: [{type: String}]
})

module.exports = Mongoose.model('users', UserSchema);
