/**
 * Created by sahil on 8/2/16.
 */

var async = require('async');
var constants = require('../../Config/constants');
var createError = require('../../Util/createError');
var createSuccess = require('../../Util/createSuccess');
var authenController = require('../AuthenController/authenController');
var authorizeController = require('../AuthenController/authenController').authorize;
var service = require('../../Service/databaseQueries');
var userModel = require('../../Model/userModel');
var tweetModel = require('../../Model/tweetModel');
var errorMessages = require('../../Config/responseMessages').errorMessages;
var successMessages = require('../../Config/responseMessages').successMessages;
var statusCodes = constants.STATUS_CODES;
var handleError = require('../../Util/handleError');
var getUTCDate = require('../../Util/getUTCDate');
var fs = require('fs');
var FILE_PATHS = constants.FILE_PATHS;
var fileType = require('file-type');
var PIC_EXTS = constants.PIC_EXTS;
var moment = require('moment');
var FOLLOW_OPTIONS = require('../../Config/constants').FOLLOW_OPTIONS;
var LIKE_OPTIONS = require('../../Config/constants').LIKE_OPTIONS;
var gcm = require('node-gcm');
/*
 ----------------------------------------
 VIEW USER PROFILE CONTROLLER
 ----------------------------------------
 */

var editUserInfo = function(token, payload, callback) {

    async.waterfall([

        function(callback) {
            authenController.authorize(token, userModel, {_id: 1}, callback);
        },

        function(result, callback) {
            service.updateData(userModel, {_id: result._id}, payload, {}, callback);
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(null, createSuccess(statusCodes.OK, successMessages.INFO_UPDATED, null));
    })

}

/*
 ----------------------------------------
  FOLLOW UNFOLLOW CONTROLLER
 ----------------------------------------
 */

var followUser = function(token, payload, callback) {

    async.waterfall([

        function (callback) {/*authorize user*/
            authorizeController(token, userModel, {_id: 1}, callback);
        },

        function(result, callback) {/*verify if followee exists*/

            service.findOneData(userModel, {userid: payload.followee, isDeleted: false}, {_id: 1}, function(err, res) {
                if (err) {
                    return callback(err);
                }
                if (!res) {
                    return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_NOT_FOUND));
                }
                return callback(null, result._id, res._id);
            });
        },

        function(_userId, _followeeId, callback) {/*update followee's follower*/
            service.updateData(userModel, {_id: _followeeId}, {$addToSet: {followers: _userId}}, {}, function (err, res) {
                if (err) {
                    return callback(err);
                }
                return callback(null, res, _userId, _followeeId);
            });
        },

        function (result, _userId, _followeeId, callback) {/*update user's following*/
            if (result.ok === 0) {
                return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_NOT_FOUND));
            }
            if (result.nModified === 0) {
                return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_ALREADY_FOLLOWED));
            }
            if (result.nModified === 1) {
                return service.updateData(userModel, {_id: _userId}, {$addToSet: {following: _followeeId}}, {}, callback);
            }
        }

    ], function (err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(null, createSuccess(statusCodes.CREATED, successMessages.FOLLOWED, null));
    });

}

var unfollowUser = function(token, payload, callback) {

    async.waterfall([

        function (callback) {/*authorize user*/
            authorizeController(token, userModel, {_id: 1}, callback);
        },

        function(result, callback) {/*verify followee exists*/
            service.findOneData(userModel, {userid: payload.followee, isDeleted: false}, {_id: 1}, function(err, res) {
                if (err) {
                    return callback(err);
                }
                if(!res) {
                    return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_NOT_FOUND))
                }
                return callback(null, result._id, res._id);
            });
        },

        function(_userId, _followeeId, callback) {/*update followee's follower*/
            service.updateData(userModel, {_id: _followeeId}, {$pull: {followers: _userId}}, {}, function (err, res) {
                if (err) {
                    return callback(err);
                }
                return callback(null, res, _userId, _followeeId);
            });
        },

        function (result, _userId, _followeeId, callback) {/*update user's following*/
            if (result.ok === 0) {
                return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_NOT_FOUND));
            }
            if (result.nModified === 0) {
                return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_ALREADY_NOT_FOLLOWED));
            }
            if (result.nModified === 1) {
                return service.updateData(userModel, {_id: _userId}, {$pull: {following: _followeeId}}, {}, callback);
            }
        }

    ], function (err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(null, createSuccess(statusCodes.CREATED, successMessages.UNFOLLOWED, null));
    });

}

var followUnfollowUser = function(token, payload, callback) {
    if (payload.action === FOLLOW_OPTIONS.FOLLOW) {
        followUser(token, payload, callback);
    }
    if (payload.action === FOLLOW_OPTIONS.UNFOLLOW) {
        unfollowUser(token, payload, callback);
    }
}

/*
 ----------------------------------------
 TIMELINE CONTROLLER
 ----------------------------------------
 */

var getTimeline = function(token, callback) {

    async.waterfall([

        function(callback){/*authorize*/
            authorizeController(token, userModel, {_id: 1, following: 1}, callback);
        },

        function(result, callback) {
            result.following.push(result._id);
            var pipeline = [
                {"$match" : {$or: [{"createdBy": {$in : result.following}}, {"postedBy": {$in: result.following}}]}},
                {"$project": {"tweets": 1, "_id": 1, createdBy: 1, postedBy: 1, text: 1}},
                {"$unwind": "$postedBy"},
                {"$match" : {$or: [{"createdBy": {$in : result.following}}, {"postedBy": {$in: result.following}}]}}
            ];

            var populateOptions = {
                match: {isDeleted: false},
                path: 'postedBy createdBy',
                model: 'users',
                select: 'firstname -_id',
            }
            service.aggregateAndPopulateData(tweetModel, pipeline, populateOptions, callback);
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        console.log(result);
        return callback(null, createSuccess(statusCodes.OK, successMessages.TWEET_EXTRACTED, result));
    });

}

/*
 ----------------------------------------
 POST TWEET CONTROLLER
 ----------------------------------------
 */

var postTweet = function(token, payload, callback) {

    async.waterfall([

        function(callback){/*authorize*/
            authorizeController(token, userModel, {_id: 1}, callback);
        },

        function(result, callback){/*create new tweet*/
            service.addData(tweetModel, {createdBy: result._id, postedBy: result._id, likes: 0, text: payload.text, timeCreated: getUTCDate(new Date())}, callback);
        },//getUTCDate(new Date())

        function(result, callback) {/*add reference of tweet in user*/
            console.log(result);
            service.updateData(userModel, {_id: result.createdBy}, {$push: {tweets: result._id}}, {}, callback);
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(null, createSuccess(statusCodes.CREATED, successMessages.TWEET_POSTED, null));

    });

}

/*
 ----------------------------------------
 VIEW USER PROFILE CONTROLLER
 ----------------------------------------
 */

var viewUserProfile = function(token, userid, callback) {

    async.waterfall([

        function(callback) {
            authorizeController(token, userModel, {_id: 1}, callback);
        },

        function(result, callback) {
            service.populateData(
                userModel,//collection
                {userid: userid},//query
                {_id: 0, firstname: 1, lastname: 1, tweets: 1},//proection
                {lean: true, sort: {timeCreated: -1}},//options
                {//populateOptions
                    path: 'tweets',
                    model: 'tweets',
                    select: 'text -_id likes timeCreated',
                },
                callback
            );
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(null, createSuccess(statusCodes.OK, successMessages.PROFILE_EXTRACTED, result));
    })

}

/*
 ----------------------------------------
 COUNT USER TWEETS CONTROLLER
 ----------------------------------------
 */

var countUserTweets = function(token, callback) {

    async.waterfall([

        function(callback) {
            authorizeController(token, userModel, {_id: 1}, callback);
        },

        function(result, callback) {
            service.aggregateData(
                userModel,
                [
                    {"$match" : {"_id": result._id}},
                    {"$project": {"tweets": 1, "_id": 0}},
                    {"$unwind": "$tweets"},
                    {"$group": {
                        _id: null,
                        "count": {"$sum": 1}
                    }}
                ],
                callback
            );
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(null, createSuccess(statusCodes.OK, successMessages.COUNT, {"count": result[0].count}));
    })
}

/*
 ----------------------------------------
 RETWEET CONTROLLER
 ----------------------------------------
 */

var retweet = function(token, payload, callback) {

    async.waterfall([

        function(callback) {
            authorizeController(token, userModel, {_id: 1}, callback);
        },

        function(result, callback) {
            service.updateData(tweetModel, {_id: payload.tweetId}, {$addToSet: {postedBy: result._id}}, {}, callback);
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        if (!result.nModified) {
            return callback(createError(statusCodes.BAD_REQUEST, errorMessages.TWEET_ALREADY_RETWEETED));
        }
        return callback(null, createSuccess(statusCodes.OK, successMessages.INFO_UPDATED, null));
    })

}

/*
 ----------------------------------------
 LIKE TWEET CONTROLLER
 ----------------------------------------
 */

var likeTweet = function(token, payload, callback) {

    async.waterfall([

        function(callback) {
            authorizeController(token, userModel, {_id: 1}, callback);
        },

        function(result, callback) {
            service.updateData(tweetModel, {_id: payload.tweetId}, {$addToSet: {likedBy: result._id}}, {}, callback);
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        if (!result.ok) {
            return callback(createError(statusCodes.BAD_REQUEST, errorMessages.TWEET_NOT_FOUND));
        }
        if (!result.nModified) {
            return callback(createError(statusCodes.BAD_REQUEST, errorMessages.TWEET_ALREADY_LIKED));
        }
        return callback(null, createSuccess(statusCodes.OK, successMessages.TWEET_LIKED, null));
    })

}

var unlikeTweet = function(token, payload, callback) {

    async.waterfall([

        function(callback) {
            authorizeController(token, userModel, {_id: 1}, callback);
        },

        function(result, callback) {
            service.updateData(tweetModel, {_id: payload.tweetId}, {$pull: {likedBy: result._id}}, {}, callback);
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        if (!result.ok) {
            return callback(createError(statusCodes.BAD_REQUEST, errorMessages.TWEET_NOT_FOUND));
        }
        if (!result.nModified) {
            return callback(createError(statusCodes.BAD_REQUEST, errorMessages.TWEET_ALREADY_UNLIKED));
        }
        return callback(null, createSuccess(statusCodes.OK, successMessages.TWEET_UNLIKED, null));
    })

}

var likeUnlikeTweet = function(token, payload, callback) {
    if (payload.action === LIKE_OPTIONS.LIKE) {
        return likeTweet(token, payload, callback);
    }else {
        return unlikeTweet(token, payload, callback);
    }
}

/*
 ----------------------------------------
 UPLOAD DISPLAY PIC CONTROLLER
 ----------------------------------------
 */

var uploadDP = function(token, payload, callback) {
    var fileExt = fileType(payload.file._data).ext;

    if (!PIC_EXTS[fileExt]) {/*check file extension*/
        return callback(createError(statusCodes.BAD_REQUEST, errorMessages.INVALID_FILE));
    }

    async.waterfall([

        function (callback) {
            authorizeController(token, userModel, {_id: 1}, callback);
        },

        function(result, callback) {
            var path = FILE_PATHS.PROFILE_PICS + result._id + "." + fileExt;
            var file = fs.createWriteStream(path);
            file.on('error', function (err) {
                return callback(err);
            });
            payload.file.pipe(file);
            callback(null, result, path);
        },

        function(result, path, callback) {
            service.updateData(userModel, {_id: result._id}, {$set: {'profilePic': path}}, {lean: true, upsert: true}, callback);
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        callback(null, createSuccess(statusCodes.OK, successMessages.PIC_UPLOADED, null));
    });
};

/*
 ----------------------------------------
 MY TWEETS CONTROLLER
 ----------------------------------------
 */

var myTweets = function(token, payload, callback) {

    async.waterfall([

        function(callback) {
            authorizeController(token, userModel, {_id: 1}, callback);
        },

        function(result, callback) {
            service.populateData(
                tweetModel,//collection
                {$or: [{postedBy: result._id}, {createdBy: result._id}], timeCreated: {$gt: moment().subtract(payload.years, 'years').subtract(payload.months, 'months')._d}},//query
                {_id: 0, text: 1, createdBy: 1},//proection
                {lean: true, sort: {timeCreated: -1}},//options
                [
                    {//populateOptions
                        path: 'createdBy',
                        model: 'users',
                        select: 'userid -_id'
                    }
                ],
                callback
            );

        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(null, createSuccess(statusCodes.OK, successMessages.COUNT, result));
    })
}

/*
 ----------------------------------------
 MY LAST TWEETS CONTROLLER
 ----------------------------------------
 */

var myyLastTweets = function(token, payload, callback) {

    async.auto({
        authorize: function(callback) {
            authorizeController(token, userModel, {_id: 1}, callback);
        },
        lastDay: ['authorize', function(callback, result) {
            //console.log(result);
            service.populateData(
                tweetModel,//collection
                {$or: [{postedBy: result.authorize._id}, {createdBy: result.authorize._id}], timeCreated: {$gt: moment().subtract(1, 'days')._d}},//query
                {_id: 0, text: 1, createdBy: 1},//proection
                {lean: true, sort: {timeCreated: -1}},//options
                [
                    {//populateOptions
                        path: 'createdBy',
                        model: 'users',
                        select: 'userid -_id'
                    }
                ],
                callback
            );
        }],
        lastMonth: ['authorize', function(callback, result) {
            console.log(result);
            service.populateData(
                tweetModel,//collection
                {$or: [{postedBy: result.authorize._id}, {createdBy: result.authorize._id}], timeCreated: {$gt: moment().subtract(1, 'months')._d}},//query
                {_id: 0, text: 1, createdBy: 1},//proection
                {lean: true, sort: {timeCreated: -1}},//options
                [
                    {//populateOptions
                        path: 'createdBy',
                        model: 'users',
                        select: 'userid -_id'
                    }
                ],
                callback
            );
        }],
        lastYear: ['authorize', function(callback, result) {
            service.populateData(
                tweetModel,//collection
                {$or: [{postedBy: result.authorize._id}, {createdBy: result.authorize._id}], timeCreated: {$gt: moment().subtract(1, 'years')._d}},//query
                {_id: 0, text: 1, createdBy: 1},//proection
                {lean: true, sort: {timeCreated: -1}},//options
                [
                    {//populateOptions
                        path: 'createdBy',
                        model: 'users',
                        select: 'userid -_id'
                    }
                ],
                callback
            );
        }]
    }, function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        result = {
            lastDay: result.lastDay,
            lastMonth: result.lastMonth,
            lastyear: result.lastYear
        }
        return callback(null, createSuccess(statusCodes.OK, successMessages.COUNT, result));
    });
}

/*
 ----------------------------------------
 MY LAST TWEETS CONTROLLER
 ----------------------------------------
 */
var countDayWkMonYr = require('../../Util/countDayWkMonYr');

var myLastTweets = function(token, payload, callback) {
    async.waterfall([

        function(callback) {
            authorizeController(token, userModel, {_id: 1}, callback);
        },

        function(result, callback) {
            service.aggregateData(tweetModel, [
                {$match: {timeCreated: {$gt: moment().subtract(1, 'years')._d}, createdBy: result._id}},
                {$sort: {timeCreated: 1}},
                {$group: {
                    _id: {
                        day: {$dayOfMonth: '$timeCreated'},
                        month: {$month: '$timeCreated'},
                        year: {$year: '$timeCreated'}
                    },
                    count: { $sum: 1 },
                    date: { $min: '$timeCreated' }
                }}
            ], callback);
        },

        function(result, callback) {
            countDayWkMonYr(result, callback)
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        callback(null, createSuccess(statusCodes.OK, successMessages.COUNT, result));
    })
}
/*using db hits in auto*/
var myLastTweetsAuto = function(token, payload, callback) {

    async.auto({
        authorize: function(callback) {
            authorizeController(token, userModel, {_id: 1}, callback);
        },
        lastDay: ['authorize', function(callback, result) {
            service.populateData(
                tweetModel,//collection
                {$or: [{postedBy: result.authorize._id}, {createdBy: result.authorize._id}], timeCreated: {$gt: moment().subtract(1, 'days')._d}},//query
                {_id: 0, text: 1, createdBy: 1},//proection
                {lean: true, sort: {timeCreated: -1}},//options
                [
                    {//populateOptions
                        path: 'createdBy',
                        model: 'users',
                        select: 'userid -_id'
                    }
                ],
                callback
            );
        }],
        lastMonth: ['authorize', function(callback, result) {
            service.populateData(
                tweetModel,//collection
                {$or: [{postedBy: result.authorize._id}, {createdBy: result.authorize._id}], timeCreated: {$gt: moment().subtract(1, 'months')._d}},//query
                {_id: 0, text: 1, createdBy: 1},//proection
                {lean: true, sort: {timeCreated: -1}},//options
                [
                    {//populateOptions
                        path: 'createdBy',
                        model: 'users',
                        select: 'userid -_id'
                    }
                ],
                callback
            );
        }],
        lastYear: ['authorize', function(callback, result) {
            service.populateData(
                tweetModel,//collection
                {$or: [{postedBy: result.authorize._id}, {createdBy: result.authorize._id}], timeCreated: {$gt: moment().subtract(1, 'years')._d}},//query
                {_id: 0, text: 1, createdBy: 1},//proection
                {lean: true, sort: {timeCreated: -1}},//options
                [
                    {//populateOptions
                        path: 'createdBy',
                        model: 'users',
                        select: 'userid -_id'
                    }
                ],
                callback
            );
        }]
    }, function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        result = {
            lastDay: result.lastDay,
            lastMonth: result.lastMonth,
            lastyear: result.lastYear
        }
        return callback(null, createSuccess(statusCodes.OK, successMessages.COUNT, result));
    });
}

var pushNotification = function(auth, payload, callback) {
    async.waterfall([
        function(callback) {
            authorizeController(auth, userModel, {_id: 1}, callback);
        },
        function(result, callback) {
            service.findOneData(userModel, {_id: result._id}, {deviceToken: 1, _id: 0}, callback);
        },
        function(result, callback) {
            console.log('result is ');
            console.log(result);
            var message = new gcm.Message();
            message.addData('key1', payload.message);
            var sender = new gcm.Sender('AIzaSyDjXkO1EPEn38J0kGAReAKMnp_3AKkIwMk');
            var registrationTokens = result.deviceToken;

            sender.send(message, { registrationTokens: registrationTokens }, 10, function (err, response) {
                if (err) {
                    return callback(createError(statusCodes.BAD_REQUEST, errorMessages.GCM_FAILED));
                }
                console.log(response);
                callback (null);
            });
        }
    ], function(err) {
        if (err) {
            return handleError(err, callback);
        }
        return callback (null, createSuccess(statusCodes.OK, successMessages.NOTIF_PUSHED, null));
    })
}

var getMeCSV = function(callback) {
    async.waterfall([
        function(callback) {
            service.findData(userModel, {}, {_id: 0}, callback);
        },
        function(result, callback) {
            console.log(result);
            return;
        }
    ], function(err, result) {

    })

}


module.exports = {
    countUserTweets: countUserTweets,
    viewUserProfile: viewUserProfile,
    postTweet: postTweet,
    getTimeline: getTimeline,
    followUnfollowUser: followUnfollowUser,
    editUserInfo: editUserInfo,
    retweet: retweet,
    likeUnlikeTweet: likeUnlikeTweet,
    uploadDP: uploadDP,
    myTweets: myTweets,
    myLastTweets: myyLastTweets,
    pushNotification: pushNotification,
    getMeCSV: getMeCSV
}