/**
 * Created by sahil on 8/2/16.
 */

var async = require('async');
var createError = require('../../Util/createError');
var createSuccess = require('../../Util/createSuccess');
var authorizeController = require('../AuthenController/authenController').authorize;
var service = require('../../Service/index');
var userModel = require('../../Model/userModel');
var adminModel = require('../../Model/adminModel');
var FOLLOW_OPTIONS = require('../../Config/constants').FOLLOW_OPTIONS;
var ADMIN_TYPES = require('../../Config/constants').ADMIN_TYPES;
var errorMessages = require('../../Config/responseMessages').errorMessages;
var successMessages = require('../../Config/responseMessages').successMessages;
var statusCodes = require('../../Config/constants').STATUS_CODES;
var handleError = require('../../Util/handleError');
var moment = require('moment');
var USER_MODEL_PROP = require('../../Config/constants').USER_MODEL_PROP;
/*
 ----------------------------------------
 DELETE USER CONTROLLER
 ----------------------------------------
 */

var deleteUser = function (token, userId, callback) {

    async.waterfall([

        function (callback) {/*authorize admin*/
            authorizeController(token, adminModel, {_id: 0, adminType: 1}, callback);
        },

        function (result, callback) {/*delete user*/
            if (result.adminType === ADMIN_TYPES.ADMIN) {
                return callback(statusCodes.UNAUTHORIZED, errorMessages.UNAUTHORIZED);
            }
            service.findOneAndUpdateData(userModel, {userid: userId, isDeleted: false}, {$set: {isDeleted: true}}, {lean: true}, callback);
        },

        function (result, callback) {/*pull user from his followings*/
            if (!result) {
                return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_NOT_FOUND));
            }
            service.updateData(userModel, {_id: {$in : result.following}}, {$pull: {followers: result._id}}, {lean: true}, function(err, res) {
                if(err) {
                    return callback(err);
                }
                return callback(null, result._id, result.followers);
            });

        },
        function(_userid, followers, callback) {/*pull user from his followers*/
            service.updateData(userModel, {_id: {$in : followers}}, {$pull: {following: _userid}}, {lean: true}, callback);
        }

    ], function (err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(createSuccess(statusCodes.OK, successMessages.USER_DELETED, null));
    })

}

/*
 ----------------------------------------
 EDIT USER CONTROLLER
 ----------------------------------------
 */

var editUser = function(token, userid, payload, callback) {

    var pullConditions = {
        followers: USER_MODEL_PROP.followers,
        following: USER_MODEL_PROP.following
    }

    var setConditions = {
        userid: USER_MODEL_PROP.userid,
        firstname: USER_MODEL_PROP.firstname,
        lastname: USER_MODEL_PROP.lastname
    }

    var getUpdateConditions = function(payload) {
        var doc = {};
        for (var prop in payload) {
            if (pullConditions[prop]) {
                if (!doc['$pull']) {
                    doc['$pull'] = {}
                }
                doc['$pull'][prop] = payload[prop];
            }
            if (setConditions[prop]) {
                if (!doc['$set']) {
                    doc['$set'] = {}
                }
                doc['$set'][prop] = payload[prop];
            }
        }
        return doc;
    }

    var updateConditions = getUpdateConditions(payload);

    async.waterfall([

        function(callback) {/*authorize admin*/
            authorizeController(token, adminModel, {_id: 0, adminType: 1}, callback);
        },

        function(result, callback) {/*update user*/
            if (result.adminType === ADMIN_TYPES.ADMIN) {
                return callback(statusCodes.UNAUTHORIZED, errorMessages.UNAUTHORIZED);
            }
            service.updateData(userModel, {userid: userid, isDeleted: false}, updateConditions, {}, callback);
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(null, createSuccess(statusCodes.OK, successMessages.INFO_UPDATED, null));
    })
}

/*
 ----------------------------------------
 FOLLOW UNFOLLOW USER CONTROLLER
 ----------------------------------------
 */

var followUser = function(token, userId, followeeId, callback) {

    async.waterfall([

        function (callback) {/*authorize user*/
            authorizeController(token, adminModel, {_id: 0, adminType: 1}, callback);
        },

        function (result, callback) {/*verify user exists*/
            if (result.adminType === ADMIN_TYPES.ADMIN) {
                return callback(statusCodes.UNAUTHORIZED, errorMessages.UNAUTHORIZED);
            }
            service.findOneData(userModel, {userid: userId, isDeleted: false}, {_id: 1}, callback);
        },

        function(result, callback) {/*verify followee exists*/
            if (!result) {
                return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_NOT_FOUND));
            }
            service.findOneData(userModel, {userid: followeeId, isDeleted: false}, {_id: 1}, function(err, res) {
                if (!res) {
                    return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_NOT_FOUND));
                }
                if(err) {
                    return callback(err);
                }
                return callback(null, result._id, res._id)
            })
        },

        function(_userId, _followeeId, callback) {/*update followee*/
            service.updateData(userModel, {_id: _followeeId}, {$addToSet: {followers: _userId}}, {multi: true}, function (err, result) {
                if (err) {
                    return callback(err);
                }
                return callback(null, _userId, _followeeId, result);
            });
        },

        function (_userId, _followeeId, result, callback) {/*update user*/
            if (!result.ok) {/*will never be true*/
                return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_NOT_FOUND));
            }
            if (!result.nModified) {
                return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_ALREADY_FOLLOWED));
            }
            if (result.nModified === 1) {
                return service.updateData(userModel, {_id: _userId}, {$addToSet: {following: _followeeId}}, {}, callback);
            }
        }

    ], function (err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(null, createSuccess(statusCodes.CREATED, successMessages.FOLLOWED, null));
    });

}

var unfollowUser = function (token, userId, followeeId, callback) {

    async.waterfall([

        function (callback) {/*authorize user*/
            authorizeController(token, adminModel, {_id: 0, adminType: 1}, callback);
        },

        function (result, callback) {/*verify user exists*/
            if (result.adminType === ADMIN_TYPES.ADMIN) {
                return callback(statusCodes.UNAUTHORIZED, errorMessages.UNAUTHORIZED);
            }
            service.findOneData(userModel, {userid: userId, isDeleted: false}, {_id: 1}, callback);
        },

        function (result, callback) {/*verify followee exists*/
            if (!result) {
                return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_NOT_FOUND));
            }
            service.findOneData(userModel, {userid: followeeId, isDeleted: false}, {_id: 1}, function (err, res) {
                if (!res) {
                    return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_NOT_FOUND));
                }
                if (err) {
                    return callback(err);
                }
                return callback(null, result._id, res._id)
            })
        },

        function (_userId, _followeeId, callback) {/*update followee*/
            service.updateData(userModel, {_id: _followeeId, isDeleted: false}, {$pull: {followers: _userId}}, {}, function (err, result) {
                if (err) {
                    return callback(err);
                }
                return callback(null, _userId, _followeeId, result);
            });
        },

        function (_userId, _followeeId, result, callback) {/*update user*/
            if (!result.ok) {/*will never be true*/
                return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_NOT_FOUND));
            }
            if (!result.nModified) {
                return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_ALREADY_NOT_FOLLOWED));
            }
            if (result.nModified === 1) {
                return service.updateData(userModel, {_id: _userId, isDeleted: false}, {$pull: {following: _followeeId}}, {}, callback);
            }
        }

    ], function (err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(null, createSuccess(statusCodes.CREATED, successMessages.UNFOLLOWED, null));
    });

}

var followUnfollowUser = function (token, payload, callback) {
    if (payload.action === FOLLOW_OPTIONS.FOLLOW) {
        followUser(token, payload.user, payload.followee, callback);
    }
    if (payload.action === FOLLOW_OPTIONS.UNFOLLOW) {
        unfollowUser(token, payload.user, payload.followee, callback);
    }
}

/*
 ----------------------------------------
 VIEW USER PROFILE CONTROLLER
 ----------------------------------------
 */

var viewUserProfile = function(token, userid, callback) {

    async.waterfall([

        function(callback) {
            authorizeController(token, adminModel, {_id: 1}, callback);
        },

        function(result, callback) {
            service.populateData(
                userModel,//collection
                {userid: userid, isDeleted: false},//query
                {_id: 0, firstname: 1, lastname: 1, tweets: 1, followers: 1, following: 1},//proection
                {lean: true, sort: {timeCreated: -1}},//options
                [
                    {//populateOptions
                        path: 'tweets',
                        model: 'tweets',
                        select: 'text -_id likes timeCreated',
                    },
                    {
                        path: 'followers',
                        match: {isDeleted: false},
                        model: 'users',
                        select: '-_id userid'
                    },
                    {
                        path: 'following',
                        match: {isDeleted: false},
                        model: 'users',
                        select: '-_id userid'
                    }
                ],
                callback
            );
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        if (!result.length) {
            return callback(createError(statusCodes.BAD_REQUEST, errorMessages.USER_NOT_FOUND));
        }
        return callback(createSuccess(statusCodes.OK, successMessages.PROFILE_EXTRACTED, result));
    })
}

/*
 ----------------------------------------
 USER COUNT CONTROLLER
 ----------------------------------------
 */

var userCount = function(token, payload, callback) {

    async.waterfall([

        function(callback) {
            authorizeController(token, adminModel, {_id: 1}, callback);
        },

        function(result, callback) {
            service.countData(userModel, {timeCreated: {$gt: moment().subtract(payload.years, 'years').subtract(payload.months, 'months')._d}}, callback);
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(null, createSuccess(statusCodes.OK, successMessages.COUNT, result));
    })

}


module.exports = {
    deleteUser: deleteUser,
    editUser: editUser,
    followUnfollowUser: followUnfollowUser,
    viewUser: viewUserProfile,
    userCount: userCount
}