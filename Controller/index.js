/**
 * Created by sahil on 21/1/16.
 */


module.exports = {
    authenController: require('./AuthenController'),
    adminController: require('./AdminController'),
    userController: require('./UserController')
}
