/**
 * Created by sahil on 8/2/16.
 */

var bcrypt = require('../../Util/bcryptUtil');
var service = require('../../Service/databaseQueries');
var async = require('async');
var jwtCrypter = require('../../Util/JWT');
var userModel = require('../../Model/userModel');
var adminModel = require('../../Model/adminModel');
var createError = require('../../Util/createError');
var createSuccess = require('../../Util/createSuccess');
var constants = require('../../Config/constants');
var responseMessages = require('../../Config/responseMessages');
var errorMessages = responseMessages.errorMessages;
var successMessages = responseMessages.successMessages;
var statusCodes = constants.STATUS_CODES;
var handleError = require('../../Util/handleError');
var PERSONNEL_TYPES = require('../../Config/constants').PERSONNEL_TYPES;

/*
 ----------------------------------------
 AUTHORIZE CONTROLLER
 ----------------------------------------
 */

var authorize = function(token, model, projections, callback) {

    async.waterfall([

        function(callback) {/*decipher token*/
            jwtCrypter.decipherToken(token, {}, callback);
        },

        function(result, callback) {/*search the result id in users*/

            service.findOneData(model, {_id: result._id, isDeleted: false}, projections, callback);
        }

    ], function(err, result) {
        //console.log(result);
        if (err) {//error can either be jwt error or mongo error
            return handleError(err, callback);
        }
        if (!result) {
            return callback(createError(statusCodes.UNAUTHORIZED, errorMessages.USER_NOT_FOUND));
        }
        callback(null, result);

    });

}


/*
 ----------------------------------------
 LOGIN CONTROLLER
 ----------------------------------------
 */

var login = function(payload, callback) {

    var model = getModel(payload.type);
    async.auto({

        findUser: function (callback) {
            service.findOneData(model, {userid: payload.userid}, {}, callback);
        },

        matchPassword: ['findUser', function (callback, result) {

            if (!(result.findUser) || result.findUser.isDeleted) {
                return callback(createError(statusCodes.UNAUTHORIZED, errorMessages.USER_NOT_FOUND));
            }

            bcrypt.compare(payload.password, result.findUser.password, callback);

        }],

        generateToken: ['matchPassword', function (callback, result) {

            if (!result.matchPassword) {
                return callback(createError(statusCodes.UNAUTHORIZED, errorMessages.INVALID_CRED));
            }

            jwtCrypter.cipherToken({'_id': result.findUser._id, 'type' : payload.type}, function (token) {
                callback(null, token);
            })

        }],

        addToken: ['generateToken', function (callback, result) {
            service.updateData(model, {_id: result.findUser._id}, {$addToSet: {token: result.generateToken}}, {}, callback);
        }]

    }, function (err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(null, createSuccess(statusCodes.OK, successMessages.LOGGED_IN, {auth: result.generateToken}));
    });

}

var getModel = function(type) {
    switch(type) {
        case PERSONNEL_TYPES.USER:
            return userModel;
        case PERSONNEL_TYPES.ADMIN:
            return adminModel;
    }
}

/*
 ----------------------------------------
 LOGOUT CONTROLLER
 ----------------------------------------
 */

var logout = function(token, callback) {

    async.waterfall([

        function(callback) {/*decode and get _id from token*/
            jwtCrypter.decipherToken(token, {}, callback);
        },

        function(result, callback) {/*pull token from user*/
            var model = getModel(result.type);
            service.updateData(model, {_id: result._id}, {$pull: {token: token}}, {}, callback);
        }

    ], function(err, result) {//error can be jwt or update
        if (err) {
            return handleError(err, callback);
        }
        return callback(createSuccess(statusCodes.OK, successMessages.LOGGED_OUT, null));
    })

}

/*
 ----------------------------------------
 SIGNUP CONTROLLER
 ----------------------------------------
 */

var getSignupDoc = function(type, payload) {

    switch(type) {
        case PERSONNEL_TYPES.USER:
            return {
                userid: payload.userid,
                firstname: payload.firstname,
                lastname: payload.lastname,
                password: payload.password
            }
        case PERSONNEL_TYPES.ADMIN:
            return {
                userid: payload.userid,
                password: payload.password,
                firstname: payload.firstname,
                lastname: payload.lastname,
                admintype: payload.adminType
            }
    }
}

var signup = function(token, payload, type, callback) {

    var doc = getSignupDoc(type, payload);
    var model = getModel(type);

    async.waterfall([

        function(callback) {
            bcrypt.hash(payload.password, callback);
        },

        function(result, callback) {
            doc.password = result;
            service.addData(model, doc, callback);
        }

    ], function(err, result) {
        if (err) {
            return handleError(err, callback);
        }
        return callback(createSuccess(statusCodes.OK, successMessages.SIGNED_UP, null));
    })

}

module.exports = {
    login: login,
    logout: logout,
    authorize: authorize,
    signup: signup
}