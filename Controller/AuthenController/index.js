/**
 * Created by sahil on 23/1/16.
 */
var authenController = require('./authenController');

for (var controller in authenController) {
    module.exports[controller] = authenController[controller];
}
