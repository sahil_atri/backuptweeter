/**
 * Created by Safer on 12-01-2016.
 */
'use strict'
const Hapi = require('hapi');
var plugins = require('./Plugins');
var serverConfig = require('./Config').serverConfig;
var Routes = require('./Routes');

var Db = require('./Service/database');
const server = new Hapi.Server();

var connectionOptions = {
    //host: serverConfig.host.LOCAL,
    port: serverConfig.port.TEST
};


server.connection(connectionOptions);
server.route(Routes);

server.register(
    plugins,
    function(err) {
        server.start(
            function() {
                console.log('Server running at:', server.info.uri);
            }
        );
    }
);

//////////////////////////////

var x = {
    "customer_id": "56973bc2bd4c87cd4fb1daa0",
    "customer_access_token": "29973b43c3cd2f2adb27a5765ce265b7",
    "sub_category_id": "568e27420170be5620a7d018",
    "service_provider_ids": ["56974f50a87042997bf677c0", "56a1d1960fb7502e1f512fa7", "56a8a9cefcc48b720ca6e0b6", "56b1f4aeb80990bf1dd9954e", "56b2fba1b80990bf1dd99555"],
    "project_type": 0,
    "location": {
        "locationType": "TOWN",
        "StateName": "Chandigarh",
        "CityName": "Chandigarh",
        "town": "Sector 28"
    },
    questionnaireForm: [
        {
            "title": "Car Type",
            "type": 2,
            "forSp": false,
            "forCp": true,
            "required": true,
            "filledValues": [
                "Sedan"
            ],
            "value": [
                "Hatchback",
                "Sedan",
                "SUV / MUV"
            ]
        },
        {
            "title": "Cleaning Type",
            "type": 1,
            "forSp": false,
            "forCp": true,
            "required": true,
            "filledValues": [
                "Interior"
            ],
            "value": [
                "Drycleaning",
                "Exterior",
                "Interior",
                "Interior & Exterior"
            ]
        },
        {
            "title": "Pick up Time",
            "type": 5,
            "forSp": false,
            "forCp": true,
            "required": true,
            "filledValues": [
                "2016-01-28 18:15:31"
            ],
            "value": []
        }
    ],
    proposalForm: [
        {
            "title": "Estimate Cost",
            "type": 7,
            "forSp": true,
            "forCp": false,
            "required": true,
            "filledValues": [
                "2558884556"
            ],
            "value": []
        },
        {
            "title": "Notes",
            "type": 0,
            "forSp": true,
            "forCp": false,
            "required": false,
            "filledValues": [
                ""
            ],
            "value": []
        }
    ]
}